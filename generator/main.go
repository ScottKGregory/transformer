package main

import (
	"flag"
	"fmt"
	"os"
	"plugin"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func main() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout})

	in := flag.String("in", "", "The file to use")

	p, err := plugin.Open(*in)
	if err != nil {
		panic(fmt.Errorf("error opening file: %v", err))
	}

	s, err := p.Lookup("Config")
	if err != nil {
		panic(fmt.Errorf("error looking up: %v", err))
	}

	log.Debug().Interface("s", s).Msg("Symbol")

	// siteCmd.Flags().String("s3.id", "", "s3.id")
	// viper.BindPFlag("s3.id", siteCmd.Flags().Lookup("s3.id"))
}
