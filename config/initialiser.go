package config

import (
	"fmt"
	"reflect"
	"strings"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func Parse(obj interface{}, cmd *cobra.Command) {
	processFields("", reflect.TypeOf(obj), cmd)
}

func processFields(prefix string, t reflect.Type, cmd *cobra.Command) {
	for i := 0; i < t.NumField(); i++ {
		processField(prefix, t.Field(i), cmd)
	}
}

func processField(prefix string, field reflect.StructField, cmd *cobra.Command) {
	tag := field.Tag.Get("config")
	n := strings.ToLower(field.Name)
	if prefix != "" {
		n = fmt.Sprintf("%s.%s", prefix, strings.ToLower(field.Name))
	}

	l := log.With().Str("name", n).Logger()
	if tag != "" {
		l = l.With().Str("tag", tag).Logger()
	}

	v := true
	var err error
	switch field.Type.Kind() {
	case reflect.Int:
		cmd.Flags().Int(n, 0, tag)
		err = viper.BindPFlag(n, cmd.Flags().Lookup(n))
	case reflect.String:
		cmd.Flags().String(n, "", tag)
		err = viper.BindPFlag(n, cmd.Flags().Lookup(n))
	case reflect.Float64:
		cmd.Flags().Float64(n, 0, tag)
		err = viper.BindPFlag(n, cmd.Flags().Lookup(n))
	case reflect.Float32:
		cmd.Flags().Float32(n, 0, tag)
		err = viper.BindPFlag(n, cmd.Flags().Lookup(n))
	case reflect.Int8:
		cmd.Flags().Int8(n, 0, tag)
		err = viper.BindPFlag(n, cmd.Flags().Lookup(n))
	case reflect.Int16:
		cmd.Flags().Int16(n, 0, tag)
		err = viper.BindPFlag(n, cmd.Flags().Lookup(n))
	case reflect.Int32:
		cmd.Flags().Int32(n, 0, tag)
		err = viper.BindPFlag(n, cmd.Flags().Lookup(n))
	case reflect.Int64:
		cmd.Flags().Int64(n, 0, tag)
		err = viper.BindPFlag(n, cmd.Flags().Lookup(n))
	case reflect.Bool:
		cmd.Flags().Bool(n, false, tag)
		err = viper.BindPFlag(n, cmd.Flags().Lookup(n))
	case reflect.Array:
		// Currently assumes string array
		cmd.Flags().StringArray(n, []string{}, tag)
		err = viper.BindPFlag(n, cmd.Flags().Lookup(n))
	case reflect.Slice:
		err = processSlice(n, tag, field, cmd)
		if err != nil && strings.HasPrefix(err.Error(), "unsupported type for slice") {
			l.Warn().Msg("Usupported slice type detected, no flag will be bound")
			err = nil
			v = false
		}
	case reflect.Map:
		l.Warn().Msg("Map value detected, no flag will be bound")
		v = false
	case reflect.Struct:
		processFields(n, field.Type, cmd)
		return
	case reflect.Ptr:
		processFields(n, field.Type.Elem(), cmd)
		return
	default:
		return
	}

	if err != nil {
		panic(err)
	}

	if v {
		l.Debug().Msg("Field registered")
	}
}

func processSlice(n, tag string, field reflect.StructField, cmd *cobra.Command) (err error) {
	switch field.Type.Elem().Kind() {
	case reflect.Int:
		cmd.Flags().IntSlice(n, []int{}, tag)
	case reflect.String:
		cmd.Flags().StringSlice(n, []string{}, tag)
	case reflect.Float64:
		cmd.Flags().Float64Slice(n, []float64{}, tag)
	case reflect.Float32:
		cmd.Flags().Float32Slice(n, []float32{}, tag)
	case reflect.Int32:
		cmd.Flags().Int32Slice(n, []int32{}, tag)
	case reflect.Int64:
		cmd.Flags().Int64Slice(n, []int64{}, tag)
	case reflect.Bool:
		cmd.Flags().BoolSlice(n, []bool{}, tag)
	case reflect.Int8,
		reflect.Int16,
		reflect.Array,
		reflect.Slice,
		reflect.Map,
		reflect.Struct,
		reflect.Ptr:
		return fmt.Errorf("unsupported type for slice: %s", n)
	default:
		return err
	}

	err = viper.BindPFlag(n, cmd.Flags().Lookup(n))
	return err
}
