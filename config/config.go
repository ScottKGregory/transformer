//go:generate ../generator/main.go -in config.go

package config

type Config struct {
	Root   string `config:"The root directory to do a thing with"`
	Number int    `config:"A number to use for testing"`
	Struct InnerConfig
	*EmbeddedConfig
}

type InnerConfig struct {
	InnerRoot        string `config:"The inner root"`
	InnerNumber      int    `config:"The inner number"`
	SuperInnerStruct SuperInnerConfig
}

type SuperInnerConfig struct {
	SuperInnerRoot   string `config:"The super inner root"`
	SuperInnerNumber int    `config:"The super inner number"`
}

type EmbeddedConfig struct {
	EmbeddedRoot   string `config:"The embedded root"`
	EmbeddedNumber int    `config:"The embedded number"`
}
