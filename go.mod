module gitlab.com/scottkgregory/transformer

go 1.15

require (
	github.com/go-delve/delve v1.6.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/rs/zerolog v1.20.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
)
