package cmd

import (
	"fmt"
	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/scottkgregory/transformer/config"
)

var cfgFile string
var cfg config.Config

var rootCmd = &cobra.Command{
	Use:   "transformer",
	Short: "",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		log.Info().
			Interface("config", cfg).
			Msg("Struct")

		log.Info().
			Interface("viper-keys", viper.GetViper().AllKeys()).
			Interface("viper-settings", viper.GetViper().AllSettings()).
			Msg("Viper")

		// log.Info().
		// 	Interface("flags", cmd.Flags().FlagUsages()).
		// 	Msg("Flags")
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout})
	config.Parse(config.Config{}, rootCmd)
	cobra.OnInitialize(initConfig)
}

func initConfig() {
	viper.Unmarshal(&cfg)
}
